window.addEventListener('keydown', handleKeydown, true);
window.addEventListener('keyup', handleKeyup, true);
const CTRL=17;
const TAB=192;
function handleKeyup(e) {
	//console.log("DEBUG: handleUp.keyCode:"+e.keyCode+"; ctrlKey=" + e.ctrlKey );
	if( e.keyCode == CTRL ) {
		safari.self.tab.dispatchMessage('stopTab');
	}
}
function handleKeydown(e) {
	//console.log("DEBUG: handleDown.keyCode:"+e.keyCode+"; ctrlKey=" + e.ctrlKey );
    if (e.keyCode == TAB && e.ctrlKey) {
        e.preventDefault();
		//e.stopImmediatePropagation();
		//e.stopPropagation();
        safari.self.tab.dispatchMessage('nextTab');
    }
}