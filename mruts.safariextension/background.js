var window2tabStack=[];
var nextTab=false;
var nextTabState=false;
var stackOverRun=false;

safari.application.addEventListener("open",traceTabsOnOpen,true);
safari.application.addEventListener("close",traceTabsOnRemoved,true);
safari.application.addEventListener("activate",traceTabsOnActivate,true);
safari.application.addEventListener("message", waitForMessage, false);
safari.extension.settings.addEventListener("change", changeSettings, false);

init();

function init(){
	changeSettings();
	var tabCount=0;
	for(var w=0; w < safari.application.browserWindows.length; w++){
		var win = safari.application.browserWindows[w];
		var active=false;
		for( var t=0; t < win.tabs.length; t++ ){
			if( win.activeTab == win.tabs[t]) active=win.tabs[t];
			else {
				pushTabToStack(win, win.tabs[t])();
			}
			tabCount++;
		}
		if( active ) pushTabToStack(win, active)();
	}
	console.log("Init finished. " + window2tabStack.length + " windows found with " + tabCount + " Tabs on them");
}
function pushTabToStack(win,tab){
	return function(){
		var isFound = false;
		for(var i=0;i<window2tabStack.length && window2tabStack[i].id != win;i++);
		if( i < window2tabStack.length ){
			var stack = window2tabStack[i].stack;
			for(var j=0;j<stack.length && stack[j] != tab;j++);
			if( j < stack.length ){
				stack.splice(j,1);
				isFound = true;
			}
			stack.push(tab);
			//console.log("DEBUG: Push tab in win "+ i +". Stack length="+ window2tabStack[i].stack.length + "; tab.url="+tab.url); 
		} else {
			window2tabStack.push({"id":win,"stack":[tab]});
			//console.log("DEBUG: Push tab in new win. tab.url="+tab.url); 	
		}
		// Remove the same Tab from the other windows
		if( !isFound ) {
			for(var i=0;i<window2tabStack.length; i++){
				if(window2tabStack[i].id != win){
					var stack = window2tabStack[i].stack;
					for(var j=0;j<stack.length && stack[j] != tab;j++);
					if( j < stack.length ){
						stack.splice(j,1);
						//console.log("DEBUG: Tab removes from the old location");
					}
				}
			}
		}
	}
}
function removeTabFromStack(win,tab){
	if( win ){
		for(var i=0;i<window2tabStack.length && window2tabStack[i].id != win;i++);
		if( i < window2tabStack.length ){
			var stack = window2tabStack[i].stack;
			for(var j=0;j<stack.length && stack[j] != tab;j++);
			if( j < stack.length ){
				stack.splice(j,1);
			}
			//console.log("DEBUG: Remove tab from win "+ i +". Stack length="+ window2tabStack[i].stack.length); 	
		} else {
			console.log("Window was not found"); 	
		}
	} else {
		// Remove window
		for(var i=0;i<window2tabStack.length && window2tabStack[i].id != tab;i++);
		if( i < window2tabStack.length ){
			window2tabStack.splice(i,1);
			//console.log("DEBUG: Window "+ i +" was removed"); 	
		} 
	}
	
}
function traceTabsOnOpen(event){
	var tab = event.target;
	var win = tab.browserWindow;
	if( win ){
		//console.log("DEBUG: Open event for tab"); 

		for(var i=0;i<window2tabStack.length && window2tabStack[i].id != win;i++);
		if( i < window2tabStack.length ){
			var stack = window2tabStack[i].stack;
			for(var j=0;j<stack.length && stack[j] != tab;j++);
			if( j >= stack.length ){
				stack.unshift(tab);
			}
		} else {
			window2tabStack.push({"id":win,"stack":[tab]});
		}
		//console.log("DEBUG: Open new tab in win "+ i +". Stack length="+ window2tabStack[i].stack.length); 
	}
}
function traceTabsOnActivate(event){
	var tab = event.target;
	var win = tab.browserWindow;
	if( !nextTabState && win ) {
		//console.log("DEBUG: Activate event for tab"); 
		pushTabToStack(win,tab)();
	}
}
function traceTabsOnRemoved(event){
	var tab = event.target;
	var win = tab.browserWindow;
	removeTabFromStack(win,tab);
}
function waitForMessage(msgEvent) {
	var tab=msgEvent.target;
	var win=tab.browserWindow;
	var name=msgEvent.name;
	var data=msgEvent.message;
	//console.log("DEBUG: onMessage: "+name);
	if( "nextTab" === name ){
		for(var i=0;i<window2tabStack.length && window2tabStack[i].id != win;i++);
		if( i < window2tabStack.length ){
			var stack = window2tabStack[i].stack;
			var tabToActivate=false;
			while( stack.length > 1 && !tabToActivate ) {
				if( nextTabState ) {
					nextTab -= 1;
					if( nextTab < 0 ) {
						nextTab = stack.length - 1;
						stackOverRun = true;
					}
				} else {
					nextTabState = true;
					nextTab = stack.length - 2;
					stackOverRun = false;
				}
				
				tabToActivate=stack[nextTab];
				// Remove broken Tabs to workaround lack of Close event on moving Tab to the new Window
				if( !(tabToActivate && tabToActivate.browserWindow ) ) {
					stack.splice(nextTab,1);
					tabToActivate = false;
					//console.log("DEBUG: Remove broken Tab " + nextTab + " in win=" + i + "; new length is "+ window2tabStack[i].stack.length); 
				}
			} 
			if( tabToActivate ) tabToActivate.activate();
			else console.log("Nothing to activate");
		} else {
			console.log("Window is not registered");
		}
		//console.log("DEBUG: Stack length="+ window2tabStack[i].stack.length + "; nextTab="+nextTab); 
	} else if ("stopTab" === name) {
		if( nextTabState ){
			for(var i=0;i<window2tabStack.length && window2tabStack[i].id != win;i++);
			if( i < window2tabStack.length ){
				var stack = window2tabStack[i].stack;
				if( stack.length > 0 ){
					if( stackOverRun ){
						window2tabStack[i].stack = 
							stack.slice(0,nextTab).reverse().concat(
							stack.slice(nextTab,stack.length).reverse());
					} else {
						window2tabStack[i].stack = 
							stack.slice(0,nextTab).concat(
							stack.slice(nextTab,stack.length).reverse());
					}
				} else {
					console.log("Error: stack is empty");
				}
			} else {
				console.log("Window is not registered"); 
			}
			//console.log("DEBUG: stopTab Stack length="+ window2tabStack[i].stack.length); 
			nextTab=false;
			nextTabState=false;
			stackOverRun = false;
		}
	} 
}
function changeSettings(){
	var inject=false;
	switch(safari.extension.settings.nextTabKey){
		case "cq":
			inject = "inject_cq.js";
			break;
		default:
			inject = "inject.js";
	}
	safari.extension.removeContentScripts();
	safari.extension.addContentScriptFromURL(safari.extension.baseURI+inject,["*"],[],false);
	console.log("Inject scripts defined");
}
